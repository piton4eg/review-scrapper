# Review Scrapper
Sinatra-based lending reviews scrapper (from www.lendingtree.com)

## Running on your machine:

```
gem install bundler
bundle install
bundle exec rackup config.ru -p 4567
```

## or in docker:

```
docker build -t review-scrapper .
docker run -p 4567:4567 review-scrapper
```

## Then curl it in separate console tab:

```
curl http://localhost:4567/reviews?url=https://www.lendingtree.com/reviews/personal/first-midwest-bank/52903183
```

## or open in the browser:

```
http://localhost:4567/reviews?url=https://www.lendingtree.com/reviews/personal/first-midwest-bank/52903183
```
