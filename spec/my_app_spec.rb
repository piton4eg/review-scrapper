require "./my_app"

require "rack/test"
require "rspec"

RSpec.configure do |c|
  c.include Rack::Test::Methods
end

describe "My App" do
  def app
    MyApp.new
  end

  describe "GET '/'" do
    it "displays instructions" do
      get "/"
      expect(last_response.body).to eq("Please, give me the lender's URL in :url parameter, like: /reviews?url=YOUR_URL")
    end
  end

  describe "GET '/reviews'" do
    context "when url is not specified" do
      it "displays instructions" do
        get "/reviews"
        expect(last_response.body).to eq("Please, give me the lender's URL in :url parameter, like: /reviews?url=YOUR_URL")
      end
    end

    context "when url is specified" do
      let(:scrapper) { double(execute!: response) }
      let(:response) do
        [{ title: "Some title", content: "Some content" }]
      end
      let(:url) { "https://www.lendingtree.com/reviews/personal/first-midwest-bank/52903183" }

      before do
        allow(Scrapper).to receive(:new).and_return(scrapper)
      end

      it "parses url" do
        get "/reviews?url=#{url}"
        expect(last_response.body).to eq(response.to_json)
      end
    end
  end
end
