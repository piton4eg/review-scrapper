require "./scrapper"

require "rspec"
require "vcr"

VCR.configure do |config|
  config.cassette_library_dir = "fixtures/vcr_cassettes"
  config.hook_into :webmock
end

describe Scrapper do
  describe "#execute!" do
    subject { described_class.new(url).execute! }

    context "for wrong url" do
      let(:url) { "12345" }

      it "returns error message" do
        is_expected.to eq("Something went wrong, please check that URL is correct and try again")
      end
    end

    context "for non existing url" do
      let(:url) { "https://foo.bar/" }

      it "returns error message" do
        is_expected.to eq("Something went wrong, please check that URL is correct and try again")
      end
    end

    context "for url without corresponding reviews" do
      let(:url) { "https://www.google.com/" }

      it "returns empty results" do
        VCR.use_cassette("google") do
          is_expected.to eq([])
        end
      end
    end

    context "for url with corresponding reviews" do
      let(:url) { "https://www.lendingtree.com/reviews/personal/first-midwest-bank/52903183" }
      let(:expected_response) do
        [
          {
            :author=>"Terry",
            :city=>"Erie, PA",
            :content=>
             "Really enjoyed the experience, they made it so easy. Applied for the loan on Thursday had the money deposited in my account on Tuesday",
            :date=>"March 2020",
            :rating=>"5 of 5",
            :title=>"Great place to get a loan"
          },
          {
            :author=>"Oscar",
            :city=>"Superior, WI",
            :content=>
             "Process was quick and simple from beginning to end. My loan specialist was easy to work with, I would highly recommend First Midwest Bank!",
            :date=>"March 2020",
            :rating=>"5 of 5",
            :title=>"Easy process"
          },
          {
            :author=>"Linda",
            :city=>"Mequon, WI",
            :content=>
             "Morgan was so very helpful. She stayed in touch and kept me updated at every turn.Thank you so much for this.",
            :date=>"March 2020",
            :rating=>"5 of 5",
            :title=>"Awesome Experience"
          },
          {
            :author=>"pamela",
            :city=>"Edgerton, KS",
            :content=>
             "The process was all online, fast and easy. When called they were prompt and clear. Courteous on the phone. Called when they said they would. Thank you!",
            :date=>"March 2020",
            :rating=>"5 of 5",
            :title=>"Personal loan quick and easy"
          },
          {
            :author=>"Eleanor",
            :city=>"Clinton, IA",
            :content=>
             "The individualized attention and ease of movement thru the online Documents was mostimpressive. Best of all, the process they work with assured me that my Application formonies was quickly approved and the next steps to complete clearly stated to me.",
            :date=>"March 2020",
            :rating=>"5 of 5",
            :title=>
             "Everything was personally handled from the start (with Marcela) to completion of the Loan (Marcela)."
          },
          {
            :author=>"Jeanne",
            :city=>"Yorkville, IL",
            :content=>
             "The customer service was excellent and made the process effortless. I would recommend this process to friends!",
            :date=>"March 2020",
            :rating=>"5 of 5",
            :title=>"Customer Service"
          },
          {
            :author=>"Loren",
            :city=>"Caraway, AR",
            :content=>
             "I would recommend Midwest Bank to everyone. Great rates. Excellent customer service. Deposit in 3 days.",
            :date=>"March 2020",
            :rating=>"5 of 5",
            :title=>"Wonderful experience"
          },
          {
            :author=>"Cathy",
            :city=>"Edmonton, KY",
            :content=>
             "Getting the loan that I needed was such an easy process!! The representative that worked with me was awesome!! I will definitely refer you to my friends and family!! The rates and payment options were so much more affordable than other loan companies!! I would definitely come back to your company in the future for my financial needs!!",
            :date=>"March 2020",
            :rating=>"5 of 5",
            :title=>"Great experience"
          },
          {
            :author=>"Scott",
            :city=>"Penfield, PA",
            :content=>
             "Working with First Midwest Bank on my personal loan was easy, rewarding, and helped me manage my credit. I highly recommend them.",
            :date=>"March 2020",
            :rating=>"5 of 5",
            :title=>"Exceptional Experience"
          },
          {
            :author=>"Jacob",
            :city=>"South Bend, IN",
            :content=>
             "This was my first time applying for a personal loan, and I don't think the process could have been any easier! MJ was very professional and made this experience a smooth one. Thank you so much!!!",
            :date=>"March 2020",
            :rating=>"5 of 5",
            :title=>"Very Fast and Smooth Process"
          }
        ]
      end

      it "returns correct results" do
        VCR.use_cassette("lendingtree") do
          is_expected.to eq(expected_response)
        end
      end
    end
  end
end
