require "sinatra/base"
require "sinatra/json"

require "./scrapper"

class MyApp < Sinatra::Base
  get "/reviews" do
    lender_url = params[:url]
    if lender_url.nil?
      return instructions
    end

    scrapper = Scrapper.new(lender_url)
    results = scrapper.execute!

    json results
  end

  get "*" do
    instructions
  end

  private def instructions
    "Please, give me the lender's URL in :url parameter, like: /reviews?url=YOUR_URL"
  end
end
