require "nokogiri"
require "open-uri"

class Scrapper
  CSS_PATHS = {
    # reviews list
    reviews: ".lenderReviews .row .mainReviews",
    # review details
    title: ".reviewDetail .reviewTitle",
    content: ".reviewDetail .reviewText",
    author: ".reviewDetail .consumerName",
    city: ".reviewDetail .consumerName",
    rating: ".recommended .numRec",
    date: ".reviewDetail .consumerReviewDate",
  }.freeze

  REGEX = {
    author: [/(\w+) from (.+)/, '\1'],      # Terry from Erie, PA     -> Terry
    city: [/(\w+) from (.+)/, '\2'],        # Terry from Erie, PA     -> Erie, PA
    rating: [/\((\d of \d)\)stars/, '\1'],  # (5 of 5)stars           -> 5 of 5
    date: [/Reviewed in (\w)/, '\1']        # Reviewed in March 2020  -> March 2020
  }.freeze

  def initialize(url)
    @url = url
  end

  def execute!
    reviews.map do |review|
      {
        title: parse_text(review, CSS_PATHS[:title]),
        content: parse_text(review, CSS_PATHS[:content]),
        author: parse_text(review, CSS_PATHS[:author]).gsub(*REGEX[:author]),
        city: parse_text(review, CSS_PATHS[:city]).gsub(*REGEX[:city]),
        rating: parse_text(review, CSS_PATHS[:rating]).gsub(*REGEX[:rating]),
        date: parse_text(review, CSS_PATHS[:date]).gsub(*REGEX[:date]),
      }
    end
  rescue
    "Something went wrong, please check that URL is correct and try again"
  end

  private def parse_text(element, css_path)
    # remove all extra spaces and new lines
    element.css(css_path).text.gsub(/\s+/, " ").strip
  rescue
    "[NOT FOUND]"
  end

  private def reviews
    @reviews ||= data.css(CSS_PATHS[:reviews])
  end

  private def data
    @data ||= Nokogiri::HTML(open(@url))
  end
end
